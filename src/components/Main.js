import React from "react";

const Main = (props) => {
  return <div className="app">{props.children}</div>;
};

export default Main;
