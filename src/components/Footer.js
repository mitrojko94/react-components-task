import React from "react";
import PropTypes from "prop-types";

const Footer = (props) => {
  return <footer>{props.title || "Footer title"}</footer>;
};

export default Footer;

Footer.propTypes = {
  title: PropTypes.string,
};

Footer.defaultProps = {
  title: PropTypes.string,
};
