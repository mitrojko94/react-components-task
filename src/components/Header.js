import React from "react";
import PropTypes from "prop-types";

const Header = (props) => {
  return <header>{props.title || "Header Title"}</header>;
};

export default Header;

Header.propTypes = {
  title: PropTypes.string,
};

Header.defaultProps = {
  title: PropTypes.string,
};
