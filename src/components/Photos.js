import React from "react";
import data from "../data";

const Photos = () => {
  return (
    <div className="photos">
      {data
        .map((item, index) => {
          return (
            <div key={item.id} className="title">
              <span>Title: {item.title}</span>
              <div>
                <img src={item.url} alt={item.title} height="200px" />
              </div>
            </div>
          );
        })
        .slice(0, 10)}
    </div>
  );
};

export default Photos;
