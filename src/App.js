import Header from "./components/Header";
import Main from "./components/Main";
import Footer from "./components/Footer";
import Photos from "./components/Photos";

function App() {
  return (
    <>
      <Header title="Hello World from Header" />
      <Main>
        <Photos />
      </Main>
      <Footer title="Hello World from Footer" />
    </>
  );
}

export default App;
